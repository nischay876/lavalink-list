---
title: Lavalink without SSL
description: Without SSL should work out of the box!
---

---
Hosted by @ [OGGY#9889](https://www.freelavalink.ga)
```bash
Host : lavalink.oops.wtf
Port : 2000
Password : "www.freehosts.ga"
Secure : false
```

Hosted by @ [FK](https://github.com/flkapes)
```bash
Host : lavalink.kapes.eu
Port : 2222
Password : "lavalinkplay"
Secure : false
```

Hosted by @ [timelessnesses](https://rukchadisa.live)
```bash
Host : lavalink.rukchadisa.live
Port : 8080
Password : "youshallnotpass"
Secure : false
```

Hosted by @ [AlexanderOF](https://alexanderof.xyz/2022/05/03/free-lavalink/)
```bash
Host : audio.alexanderof.xyz
Port : 2000
Password : "lavalink"
Secure : false
```

Hosted by @ [Thermal Hosting](https://thermalhosting.com)
```bash
Host : lava-ny-01.thermalhosting.com
Port : 4018
Password : thermalhosting.com
Secure : false
```

Hosted by @ [Blacky](https://blacky-dev.me/)
```bash
IP : 54.37.6.86
Port : 80
Password : "Blacky#9125"
Secure : false
```
Hosted by @ [Gamer3514](https://github.com/thegamer3514)
```bash
IP : 95.214.52.241
Port : 1623
Password : "youshallnotpass"
Secure : false
```
Hosted by @ [Gamer3514](https://github.com/thegamer3514)
```bash
IP : 95.214.52.241
Port : 4100
Password : "youshallnotpass"
Secure : false
```
